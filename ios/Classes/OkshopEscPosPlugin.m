#import "OkshopEscPosPlugin.h"
#if __has_include(<okshop_esc_pos/okshop_esc_pos-Swift.h>)
#import <okshop_esc_pos/okshop_esc_pos-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "okshop_esc_pos-Swift.h"
#endif

@implementation OkshopEscPosPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftOkshopEscPosPlugin registerWithRegistrar:registrar];
}
@end
