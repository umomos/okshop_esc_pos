import 'package:esc_pos_utils/esc_pos_utils.dart';

class TextArgs {
  final String text;
  num width;
  PosStyles styles;
  bool containsChinese;

  TextArgs(
    this.text, {
    this.styles,
    this.width,
    this.containsChinese = false,
  });
}
