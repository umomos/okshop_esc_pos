import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui' as ui show Image, ImageByteFormat;

import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter/material.dart' hide Image;
import 'package:flutter/rendering.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:image/image.dart';
import 'package:okshop_esc_pos/okshop_esc_pos.dart';
import 'package:okshop_model/okshop_model.dart';

const jsonString = '''
{
    "order_number": "168",
    "type": 1,
    "meat_at": "2017-12-01 18:32:00",
    "seat": "A區15桌",
    "item_name": "商品一批",
    "comment": "•2倍泡菜 •不辣",
    "unit_price": 428
}
''';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  GlobalKey globalKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  Future<void> _capturePng() async {
    RenderRepaintBoundary boundary =
        globalKey.currentContext.findRenderObject();
    if (boundary.debugNeedsPaint) {
      print("Waiting for boundary to be painted.");
      await Future.delayed(const Duration(milliseconds: 20));
      // return _capturePng();
    }
    ui.Image image = await boundary.toImage();
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    Uint8List pngBytes = byteData.buffer.asUint8List();
    print(pngBytes);
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await OkshopEscPos.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: RepaintBoundary(
          key: globalKey,
          child: Column(
            children: _children(),
          ),
        ),
      ),
    );
  }

  List<Widget> _children() {
    final children = <Widget>[];
    children.add(TextButton(onPressed: _discover, child: Text('discover')));
    children.add(TextButton(onPressed: _print, child: Text('print')));
    children.add(TextButton(onPressed: _loadItems, child: Text('loadItems')));
    children
        .add(TextButton(onPressed: _loadReceipt, child: Text('loadReceipt')));
    children
        .add(TextButton(onPressed: _loadInvoice, child: Text('loadInvoice')));
    children
        .add(TextButton(onPressed: _capturePng, child: Text('loadInvoice')));
    // children.add(Text.rich(TextSpan(
    //   style: const TextStyle(
    //     color: Colors.black,
    //     fontSize: 20,
    //     // fontWeight: ui.FontWeight.bold,
    //   ),
    //   children: [],
    // )));
    return children;
  }

  Future<void> _print() async {
    if (printers.isNotEmpty) {
      final cmd = await _demo();
      // final cmd = await _test();
      // final cmd = await _sticker();
      printers.first.rawBytes(cmd);
    }
  }

  final printers = <SettingLabel>[];

  final _printerRepository = PrinterRepository();

  void _discover() {
    // PrinterRepository.discover().then((value) {
    //   printers.clear();
    //   printers.addAll(value);
    // });
    _printerRepository.scan([PrinterType.net]).then(
      (value) {
        print('print length(${value.length})');
        printers.clear();
        printers.addAll(value);
      },
    );
  }

  // Future<List<num>> _sticker() async {
  //   final sticker = Sticker.fromRawJson(jsonString);
  //   final profile = await CapabilityProfile.load();
  //   final generator = Generator(PaperSize.mm80, profile);
  //   var bytes = <int>[];
  //   bytes += generator.sticker(sticker);
  //   bytes += generator.cut();
  //   return bytes;
  // }

  Future<List<num>> _test() async {
    final profile = await CapabilityProfile.load();
    final generator = Generator(PaperSize.mm80, profile);
    var bytes = <int>[];
    bytes += generator.init();
    bytes += generator.alignRight();
    bytes += generator.reverseOn();
    bytes += generator.boldOn();
    bytes += generator.underlineOn();
    bytes += utf8.encode('反白測試');
    bytes += generator.lf();
    bytes += generator.cut();
    return bytes;
  }

  Future<List<int>> _demo() async {
    final profile = await CapabilityProfile.load();
    final generator = Generator(PaperSize.mm58, profile);
    List<int> bytes = [];

    bytes += generator.text(
        'Regular: aA bB cC dD eE fF gG hH iI jJ kK lL mM nN oO pP qQ rR sS tT uU vV wW xX yY zZ');
    bytes += generator.text('Special 1: àÀ èÈ éÉ ûÛ üÜ çÇ ôÔ',
        styles: PosStyles(codeTable: 'CP1252'));
    bytes += generator.text('Special 2: blåbærgrød',
        styles: PosStyles(codeTable: 'CP1252'));

    bytes += generator.text('Bold text', styles: PosStyles(bold: true));
    bytes += generator.text('Reverse text', styles: PosStyles(reverse: true));
    bytes += generator.text('Underlined text',
        styles: PosStyles(underline: true), linesAfter: 1);
    bytes +=
        generator.text('Align left', styles: PosStyles(align: PosAlign.left));
    bytes += generator.text('Align center',
        styles: PosStyles(align: PosAlign.center));
    bytes += generator.text('Align right',
        styles: PosStyles(align: PosAlign.right), linesAfter: 1);

    bytes += generator.row([
      PosColumn(
        text: 'col3',
        width: 3,
        styles: PosStyles(align: PosAlign.center, underline: true),
      ),
      PosColumn(
        text: 'col6',
        width: 6,
        styles: PosStyles(align: PosAlign.center, underline: true),
      ),
      PosColumn(
        text: 'col3',
        width: 3,
        styles: PosStyles(align: PosAlign.center, underline: true),
      ),
    ]);

    bytes += generator.text('Text size 200%',
        styles: PosStyles(
          height: PosTextSize.size2,
          width: PosTextSize.size2,
        ));

    // Print image:
    final ByteData data = await rootBundle.load('assets/logo.png');
    final Uint8List imgBytes = data.buffer.asUint8List();
    final Image image = decodeImage(imgBytes);
    bytes += generator.image(image);
    // Print image using an alternative (obsolette) command
    // bytes += generator.imageRaster(image);

    // Print barcode
    final List<int> barData = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 4];
    bytes += generator.barcode(Barcode.upcA(barData));

    // Print mixed (chinese + latin) text. Only for printers supporting Kanji mode
    // ticket.text(
    //   'hello ! 中文字 # world @ éphémère &',
    //   styles: PosStyles(codeTable: PosCodeTable.westEur),
    //   containsChinese: true,
    // );

    bytes += generator.feed(2);
    bytes += generator.cut();
    return bytes;
  }

  void _loadReceipt() async {
    try {
      final receipt = await rootBundle
          .loadString('assets/receipt.json')
          .then((value) => Receipt.fromRawJson(value));
      final invoice = await rootBundle
          .loadString('assets/invoice.json')
          .then((value) => Invoice.fromRawJson(value));
      final profile = await CapabilityProfile.load();
      final generator = Generator(PaperSize.mm80, profile);
      // generator.setGlobalCodeTable('Unknown');
      var bytes = <int>[];
      bytes += generator.setStyles(
        PosStyles.defaults(
            // fontType: PosFontType.fontA,
            // codeTable: 'Unknown',
            ),
        isKanji: true,
      );
      // bytes.addAll([28, 40, 67, 2, 0, 48]);
      // bytes += await generator.textImage('我是中文');
      // bytes += generator.item(receipt.items.first);
      bytes += generator.receiptLite(receipt);
      // bytes += generator.feed(1);
      // bytes += generator.cut();
      // bytes += generator.cut(mode: PosCutMode.partial);
      printers.first.rawBytes(bytes, isKanji: true);
    } catch (e) {
      throw e;
    }
  }

  void _loadItems() async {
    try {
      final receipt = await rootBundle
          .loadString('assets/receipt.json')
          .then((value) => Receipt.fromRawJson(value));
      final invoice = await rootBundle
          .loadString('assets/invoice.json')
          .then((value) => Invoice.fromRawJson(value));
      final profile = await CapabilityProfile.load();
      final generator = Generator(PaperSize.mm58, profile);
      // generator.setGlobalCodeTable('Unknown');
      var bytes = <int>[];
      bytes += generator.setStyles(
        PosStyles.defaults(
            // fontType: PosFontType.fontA,
            // codeTable: 'Unknown',
            ),
        isKanji: true,
      );
      // bytes.addAll([28, 40, 67, 2, 0, 48]);
      // bytes += await generator.textImage('我是中文');
      // bytes += generator.item(receipt.items.first);
      bytes += generator.receiptItem(receipt);
      // bytes += generator.feed(1);
      // bytes += generator.cut();
      // bytes += generator.cut(mode: PosCutMode.partial);
      printers.first.rawBytes(bytes, isKanji: true);
    } catch (e) {
      throw e;
    }
  }

  void _loadInvoice() async {
    try {
      final receipt = await rootBundle
          .loadString('assets/receipt.json')
          .then((value) => Receipt.fromRawJson(value));
      final invoice = await rootBundle
          .loadString('assets/invoice.json')
          .then((value) => Invoice.fromRawJson(value));
      final profile = await CapabilityProfile.load();
      final generator = Generator(PaperSize.mm58, profile);
      // generator.setGlobalCodeTable('Unknown');
      var bytes = <int>[];
      bytes += generator.setStyles(
        PosStyles.defaults(
            // fontType: PosFontType.fontA,
            // codeTable: 'Unknown',
            ),
        isKanji: true,
      );
      // bytes.addAll([28, 40, 67, 2, 0, 48]);
      // bytes += await generator.textImage('我是中文');
      // bytes += generator.item(receipt.items.first);
      bytes += generator.invoice(invoice);
      // bytes += generator.feed(1);
      // bytes += generator.cut();
      // bytes += generator.cut(mode: PosCutMode.partial);
      printers.first.rawBytes(bytes, isKanji: true);
    } catch (e) {
      throw e;
    }
  }
}
