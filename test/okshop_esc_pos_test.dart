import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_esc_pos/okshop_esc_pos.dart';

void main() {
  const MethodChannel channel = MethodChannel('okshop_esc_pos');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await OkshopEscPos.platformVersion, '42');
  });

  test('print type: invoice', () {
    expect(PrintType.invoice.value, 812216666);
  });

  test('print type: receipt lite', () {
    expect(PrintType.receiptLite.value, 379469386);
  });

  test('print type: receipt item', () {
    expect(PrintType.receiptItem.value, 398818853);
  });

  test('print type: sticker', () {
    expect(PrintType.sticker.value, 583076660);
  });

  test('print type: report statements', () {
    expect(PrintType.reportStatements.value, 848362527);
  });

  test('print type: report sales', () {
    expect(PrintType.reportSales.value, 276239282);
  });
}
