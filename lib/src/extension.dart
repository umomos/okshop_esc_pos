import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:esc_pos_bluetooth/esc_pos_bluetooth.dart';
import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter/material.dart' show Colors;
import 'package:flutter/painting.dart';
import 'package:flutter_bluetooth_basic/flutter_bluetooth_basic.dart';
import 'package:image/image.dart';
import 'package:okshop_common/okshop_common.dart';
import 'package:okshop_model/okshop_model.dart';
import 'package:ping_discover_network/ping_discover_network.dart';

import 'enums.dart';
// import 'sticker.dart';
import 'text_args.dart';

extension ExtensionSettingLabel on SettingLabel {
  Future<List<int>> invoiceTask(Invoice invoice, [Uint8List imageBytes]) async {
    try {
      final profile = await CapabilityProfile.load();
      final Generator generate = Generator(PaperSize.mm58, profile);
      var bytes = generate.reset();
      bytes += generate.invoice(invoice, imageBytes);
      bytes += generate.invoiceItems(invoice);
      bytes += generate.cut(mode: PosCutMode.partial);
      return bytes;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<int>> stickerTask(Sticker sticker) async {
    final profile = await CapabilityProfile.load();
    final Generator generate = Generator(PaperSize.mm80, profile);
    var bytes = generate.reset();
    bytes += generate.sticker(sticker, PaperSize.init(printerPaperSize.index));
    bytes += generate.cut(mode: PosCutMode.partial);
    return bytes;
  }

  Future<List<int>> imageTask(Uint8List imageBytes) async {
    final profile = await CapabilityProfile.load();
    final generate = Generator(PaperSize.mm80, profile);
    var bytes = generate.reset();
    final image = decodeImage(imageBytes);
    bytes += generate.image(image);
    bytes += generate.cut(mode: PosCutMode.partial);
    return bytes;
  }

  Future<PosPrintResult> printImage(Uint8List imageBytes) async {
    final bytes = await imageTask(imageBytes);
    return rawBytes(bytes, isKanji: true);
  }

  // Future<PosPrintResult> printSticker(Sticker sticker) async {
  //   final profile = await CapabilityProfile.load();
  //   final Generator generate = Generator(PaperSize.mm80, profile);
  //   var bytes = generate.reset();
  //   bytes += generate.sticker(sticker);
  //   bytes += generate.cut(mode: PosCutMode.partial);
  //   return rawBytes(bytes, isKanji: true);
  // }

  Future<List<int>> receiptLiteTask(Receipt receipt) async {
    final profile = await CapabilityProfile.load();
    final Generator generate = Generator(PaperSize.mm80, profile);
    var bytes = generate.reset();
    bytes +=
        generate.receiptLite(receipt, PaperSize.init(printerPaperSize.index));
    bytes += generate.cut();
    return bytes;
  }

  Future<PosPrintResult> printReceiptLite(Receipt receipt) async {
    final bytes = await receiptLiteTask(receipt);
    return rawBytes(bytes, isKanji: true);
  }

  Future<List<int>> receiptItemTask(Receipt receipt) async {
    final profile = await CapabilityProfile.load();
    final Generator generate = Generator(PaperSize.mm80, profile);
    var bytes = generate.reset();
    bytes +=
        generate.receiptItem(receipt, PaperSize.init(printerPaperSize.index));
    bytes += generate.cut(mode: PosCutMode.partial);
    return bytes;
  }

  Future<PosPrintResult> printReceiptItem(Receipt receipt) async {
    final bytes = await receiptItemTask(receipt);
    return rawBytes(bytes, isKanji: true);
  }

  Future<PosPrintResult> printTestPage([PaperSize paperSize]) async {
    final profile = await CapabilityProfile.load();
    final bytes = await _testTicket(paperSize ?? PaperSize.mm58, profile);
    // final bytes = await _demoReceipt(PaperSize.mm80, profile);
    return rawBytes(bytes);
  }

  Future<List<int>> _demoReceipt(
      PaperSize paper, CapabilityProfile profile) async {
    final Generator ticket = Generator(paper, profile);
    List<int> bytes = [];

    // Print image
    // final ByteData data = await rootBundle.load('assets/rabbit_black.jpg');
    // final Uint8List imageBytes = data.buffer.asUint8List();
    // final Image? image = decodeImage(imageBytes);
    // bytes += ticket.image(image);

    bytes += ticket.text('GROCERYLY',
        styles: PosStyles(
          align: PosAlign.center,
          height: PosTextSize.size2,
          width: PosTextSize.size2,
        ),
        linesAfter: 1);

    bytes += ticket.text('889  Watson Lane',
        styles: PosStyles(align: PosAlign.center));
    bytes += ticket.text('New Braunfels, TX',
        styles: PosStyles(align: PosAlign.center));
    bytes += ticket.text('Tel: 830-221-1234',
        styles: PosStyles(align: PosAlign.center));
    bytes += ticket.text('Web: www.example.com',
        styles: PosStyles(align: PosAlign.center), linesAfter: 1);

    bytes += ticket.hr();
    bytes += ticket.row([
      PosColumn(text: 'Qty', width: 1),
      PosColumn(text: 'Item', width: 7),
      PosColumn(
          text: 'Price', width: 2, styles: PosStyles(align: PosAlign.right)),
      PosColumn(
          text: 'Total', width: 2, styles: PosStyles(align: PosAlign.right)),
    ]);

    bytes += ticket.row([
      PosColumn(text: '2', width: 1),
      PosColumn(text: 'ONION RINGS', width: 7),
      PosColumn(
          text: '0.99', width: 2, styles: PosStyles(align: PosAlign.right)),
      PosColumn(
          text: '1.98', width: 2, styles: PosStyles(align: PosAlign.right)),
    ]);
    bytes += ticket.row([
      PosColumn(text: '1', width: 1),
      PosColumn(text: 'PIZZA', width: 7),
      PosColumn(
          text: '3.45', width: 2, styles: PosStyles(align: PosAlign.right)),
      PosColumn(
          text: '3.45', width: 2, styles: PosStyles(align: PosAlign.right)),
    ]);
    bytes += ticket.row([
      PosColumn(text: '1', width: 1),
      PosColumn(text: 'SPRING ROLLS', width: 7),
      PosColumn(
          text: '2.99', width: 2, styles: PosStyles(align: PosAlign.right)),
      PosColumn(
          text: '2.99', width: 2, styles: PosStyles(align: PosAlign.right)),
    ]);
    bytes += ticket.row([
      PosColumn(text: '3', width: 1),
      PosColumn(text: 'CRUNCHY STICKS', width: 7),
      PosColumn(
          text: '0.85', width: 2, styles: PosStyles(align: PosAlign.right)),
      PosColumn(
          text: '2.55', width: 2, styles: PosStyles(align: PosAlign.right)),
    ]);
    bytes += ticket.hr();

    bytes += ticket.row([
      PosColumn(
          text: 'TOTAL',
          width: 6,
          styles: PosStyles(
            height: PosTextSize.size2,
            width: PosTextSize.size2,
          )),
      PosColumn(
          text: '\$10.97',
          width: 6,
          styles: PosStyles(
            align: PosAlign.right,
            height: PosTextSize.size2,
            width: PosTextSize.size2,
          )),
    ]);

    bytes += ticket.hr(ch: '=', linesAfter: 1);

    bytes += ticket.row([
      PosColumn(
          text: 'Cash',
          width: 7,
          styles: PosStyles(align: PosAlign.right, width: PosTextSize.size2)),
      PosColumn(
          text: '\$15.00',
          width: 5,
          styles: PosStyles(align: PosAlign.right, width: PosTextSize.size2)),
    ]);
    bytes += ticket.row([
      PosColumn(
          text: 'Change',
          width: 7,
          styles: PosStyles(align: PosAlign.right, width: PosTextSize.size2)),
      PosColumn(
          text: '\$4.03',
          width: 5,
          styles: PosStyles(align: PosAlign.right, width: PosTextSize.size2)),
    ]);

    bytes += ticket.feed(2);
    bytes += ticket.text('Thank you!',
        styles: PosStyles(align: PosAlign.center, bold: true));

    final now = DateTime.now();
    // final formatter = DateFormat('MM/dd/yyyy H:m');
    // final String timestamp = formatter.format(now);
    // bytes += ticket.text(timestamp,
    //     styles: PosStyles(align: PosAlign.center), linesAfter: 2);

    // Print QR Code from image
    // try {
    //   const String qrData = 'example.com';
    //   const double qrSize = 200;
    //   final uiImg = await QrPainter(
    //     data: qrData,
    //     version: QrVersions.auto,
    //     gapless: false,
    //   ).toImageData(qrSize);
    //   final dir = await getTemporaryDirectory();
    //   final pathName = '${dir.path}/qr_tmp.png';
    //   final qrFile = File(pathName);
    //   final imgFile = await qrFile.writeAsBytes(uiImg.buffer.asUint8List());
    //   final img = decodeImage(imgFile.readAsBytesSync());

    //   bytes += ticket.image(img);
    // } catch (e) {
    //   print(e);
    // }

    // Print QR Code using native function
    // bytes += ticket.qrcode('example.com');

    ticket.feed(2);
    ticket.cut();
    return bytes;
  }

  Future<List<int>> _testTicket(
      PaperSize paperSize, CapabilityProfile profile) async {
    final Generator generator = Generator(PaperSize.mm80, profile);
    final mm58 = PaperSize.mm58.value == paperSize.value;
    final widthSize = mm58 ? PosTextSize.size1 : PosTextSize.size2;
    final heightSize = mm58 ? PosTextSize.size1 : PosTextSize.size2;
    List<int> bytes = [];
    bytes += generator.reset();
    bytes += generator.setStyles(
      PosStyles(
        width: widthSize,
        height: heightSize,
        align: PosAlign.left,
        fontType: PosFontType.fontA,
      ),
      isKanji: false,
    );
    bytes += generator.text(
      'Regular: aA bB cC dD eE fF gG hH iI jJ kK lL mM nN oO pP qQ rR sS tT uU vV wW xX yY zZ',
      styles: PosStyles(
        bold: false,
        align: PosAlign.left,
        width: widthSize,
        height: heightSize,
        fontType: PosFontType.fontA,
      ),
    );
    // bytes += generator.text('Special 1: àÀ èÈ éÉ ûÛ üÜ çÇ ôÔ',
    //     styles: PosStyles(codeTable: PosCodeTable.westEur));
    // bytes += generator.text('Special 2: blåbærgrød',
    //     styles: PosStyles(codeTable: PosCodeTable.westEur));

    bytes += generator.text(
      'Bold text',
      styles: PosStyles(
        bold: true,
        align: PosAlign.left,
        width: widthSize,
        height: heightSize,
      ),
    );
    bytes += generator.text(
      'Reverse text',
      styles: PosStyles(
        reverse: true,
        align: PosAlign.left,
        width: widthSize,
        height: heightSize,
      ),
    );
    bytes += generator.text(
      'Underlined text',
      styles: PosStyles(
        underline: true,
        align: PosAlign.left,
        width: widthSize,
        height: heightSize,
      ),
      linesAfter: 1,
    );
    bytes += generator.text(
      'Align left',
      styles: PosStyles(
        align: PosAlign.left,
        width: widthSize,
        height: heightSize,
      ),
    );
    bytes += generator.text(
      'Align center',
      styles: PosStyles(
        align: PosAlign.center,
        width: widthSize,
        height: heightSize,
      ),
    );
    bytes += generator.text(
      'Align right',
      styles: PosStyles(
        align: PosAlign.right,
        width: widthSize,
        height: heightSize,
      ),
      linesAfter: 1,
    );

    bytes += generator.row([
      PosColumn(
        text: 'col3',
        width: 3,
        styles: PosStyles(
          align: PosAlign.center,
          underline: true,
          width: widthSize,
          height: heightSize,
        ),
      ),
      PosColumn(
        text: 'col6',
        width: 6,
        styles: PosStyles(
          align: PosAlign.center,
          underline: true,
          width: widthSize,
          height: heightSize,
        ),
      ),
      PosColumn(
        text: 'col3',
        width: 3,
        styles: PosStyles(
          align: PosAlign.center,
          underline: true,
          width: widthSize,
          height: heightSize,
        ),
      ),
    ]);

    bytes += generator.text(
      'Text size 200%',
      styles: PosStyles(
        align: PosAlign.left,
        height: PosTextSize.size2,
        width: PosTextSize.size2,
      ),
    );

    // Print image
    // final ByteData data = await rootBundle.load('assets/logo.png');
    // final Uint8List buf = data.buffer.asUint8List();
    // final Image image = decodeImage(buf)!;
    // bytes += generator.image(image);
    // Print image using alternative commands
    // bytes += generator.imageRaster(image);
    // bytes += generator.imageRaster(image, imageFn: PosImageFn.graphics);

    // Print barcode
    // final List<int> barData = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 4];
    // bytes += generator.barcode(Barcode.upcA(barData));

    // Print mixed (chinese + latin) text. Only for printers supporting Kanji mode
    // bytes += generator.text(
    //   'hello ! 中文字 # world @ éphémère &',
    //   styles: PosStyles(codeTable: PosCodeTable.westEur),
    //   containsChinese: true,
    // );

    // bytes += generator.feed(2);

    bytes += generator.cut();
    return bytes;
  }

  Future<NetworkPrinter> _asNetworkPrinter() async {
    const paper = PaperSize.mm58;
    final profile = await CapabilityProfile.load();
    return NetworkPrinter(paper, profile);
  }

  Future<PosPrintResult> rawBytes(List<int> bytes,
      {bool isKanji = true}) async {
    if (PrinterType.net.value == type) {
      // 網路印表機
      return _rawBytesLan(bytes, isKanji: isKanji);
    } else if (PrinterType.bth.value == type) {
      // 藍牙印表機
      return _rawBytesBlueTooth(bytes);
    }
    return PosPrintResult.printerNotSelected;
  }

  PrinterBluetooth _asPrinterBluetooth() {
    final device = BluetoothDevice();
    device.name = name;
    device.address = ip;
    device.type = 3;
    return PrinterBluetooth(device);
  }

  ///
  /// 藍牙印表機
  ///
  Future<PosPrintResult> _rawBytesBlueTooth(List<int> bytes) async {
    final mgr = PrinterBluetoothManager();
    final printer = _asPrinterBluetooth();
    mgr.selectPrinter(printer);
    try {
      final ret = await mgr.writeBytes(bytes);
      return ret;
    } catch (e) {
      rethrow;
    } finally {
      await Future.delayed(Duration(milliseconds: 200));
    }
  }

  ///
  /// 網路印表機
  ///
  Future<PosPrintResult> _rawBytesLan(List<int> bytes,
      {bool isKanji = true}) async {
    NetworkPrinter printer;
    try {
      printer = await _asNetworkPrinter();
      final res = await printer.connect(
        host,
        port: port,
        timeout: Duration(milliseconds: 200),
      );
      if (PosPrintResult.success == res) {
        printer.rawBytes(bytes, isKanji: isKanji);
        await Future.delayed(Duration(milliseconds: 400), () {});
        await printer.disconnect(delayMs: 200);
      } else {
        throw res.msg;
      }
      return res;
    } catch (e) {
      throw e;
    }
  }
}

extension ExtensionGenerator on Generator {
  Uint8List init() {
    final result = Uint8List(2);
    result[0] = Option.esc.value;
    result[1] = 0x40;
    return result;
  }

  Uint8List lf() {
    final result = Uint8List(1);
    result[0] = Option.lf.value;
    return result;
  }

  Uint8List reverseOn() {
    final result = Uint8List(3);
    result[0] = Option.gs.value;
    result[1] = 0x42;
    result[2] = 0x01;
    return result;
  }

  Uint8List reverseOff() {
    final result = Uint8List(3);
    result[0] = Option.gs.value;
    result[1] = 0x42;
    result[2] = 0x00;
    return result;
  }

  Uint8List boldOn() {
    final result = Uint8List(3);
    result[0] = Option.esc.value;
    result[1] = 69;
    result[2] = 0xF;
    return result;
  }

  Uint8List boldOff() {
    final result = Uint8List(3);
    result[0] = Option.esc.value;
    result[1] = 69;
    result[2] = 0;
    return result;
  }

  Uint8List underlineOn() {
    final result = Uint8List(3);
    result[0] = Option.esc.value;
    result[1] = 45;
    result[2] = 1;
    return result;
  }

  Uint8List underlineOff() {
    final result = Uint8List(3);
    result[0] = Option.esc.value;
    result[1] = 45;
    result[2] = 0;
    return result;
  }

  Uint8List alignLeft() {
    final result = Uint8List(3);
    result[0] = Option.esc.value;
    result[1] = 97;
    result[2] = 0;
    return result;
  }

  Uint8List alignCenter() {
    final result = Uint8List(3);
    result[0] = Option.esc.value;
    result[1] = 97;
    result[2] = 1;
    return result;
  }

  Uint8List alignRight() {
    final result = Uint8List(3);
    result[0] = Option.esc.value;
    result[1] = 97;
    result[2] = 2;
    return result;
  }

  // int _getMaxCharsPerLine(PosFontType font) {
  //   if (_paperSize == PaperSize.mm58) {
  //     return (font == null || font == PosFontType.fontA) ? 32 : 42;
  //   } else {
  //     return (font == null || font == PosFontType.fontA) ? 48 : 64;
  //   }
  // }

  List<int> hr80() => hr(len: 48);

  List<int> hr58() => hr(len: 32);

  List<int> hr2(num length, [String fill = '─']) {
    var bytes = <int>[];
    final line = List.filled(length, '─').join();
    bytes += utf8.encode(line);
    bytes += feed(1);
    return bytes;
  }

  List<int> _lr(String lhs, String rhs, [PaperSize paperSize]) {
    final mm58 = PaperSize.mm58.value == paperSize.value;
    final widthSize = mm58 ? PosTextSize.size1 : PosTextSize.size2;
    final heightSize = mm58 ? PosTextSize.size1 : PosTextSize.size2;
    // var bytes = <int>[];
    // bytes += alignLeft();
    // bytes += utf8.encode(lhs);
    // bytes += alignRight();
    // bytes += utf8.encode(rhs);
    // bytes += feed(1);
    // return bytes;
    return _text(TextArgs(
      '$lhs  $rhs',
      styles: PosStyles(
        width: PosTextSize.size1,
        height: heightSize,
      ),
    ));
    // return _texts([
    //   TextArgs(
    //     lhs,
    //     styles: PosStyles.defaults(
    //       align: PosAlign.left,
    //       width: widthSize,
    //       height: heightSize,
    //     ),
    //   ),
    //   TextArgs(
    //     rhs,
    //     styles: PosStyles.defaults(
    //       align: PosAlign.left,
    //       width: widthSize,
    //       height: heightSize,
    //     ),
    //   ),
    // ]);
  }

  List<int> _texts(Iterable<TextArgs> it) {
    final count = it.length;
    if (count <= 0) {
      return <int>[];
    }
    final children = it.map((e) {
      e.width ??= (12 / count).round();
      e.styles ??= PosStyles.defaults();
      return e.asPosColumn();
    });
    return row(children.toList());
  }

  List<int> _text(TextArgs val) {
    // return _texts([val]);
    return textEncoded(
      utf8.encode(val.text ?? ''),
      styles: val.styles ?? const PosStyles(),
    );
  }

  ///
  /// 列印發票
  ///
  List<int> invoice(Invoice invoice, [Uint8List imageBytes]) {
    var bytes = <int>[];
    if (imageBytes != null) {
      // 圖片方式列印 header
      bytes += image(
        decodeImage(imageBytes),
        align: PosAlign.right,
      );
    } else {
      // 文字方式列印 header
      bytes += _text(TextArgs(
        invoice.storeName,
        styles: PosStyles.defaults(
          align: PosAlign.center,
          width: PosTextSize.size1,
          height: PosTextSize.size1,
        ),
      ));
      //
      bytes += _text(TextArgs(
        invoice.displayPrintMark,
        styles: PosStyles.defaults(
          align: PosAlign.center,
        ),
      ));
      //
      bytes += _text(TextArgs(
        invoice.displayTwDateTime,
        styles: PosStyles.defaults(
          align: PosAlign.center,
        ),
      ));
      //
      bytes += _text(TextArgs(
        invoice.displayInvoiceNumber,
        styles: PosStyles.defaults(
          align: PosAlign.center,
        ),
      ));
      //
      bytes += _texts([
        TextArgs(
          invoice.displayDateTime,
          width: 8,
          styles: PosStyles(align: PosAlign.left),
        ),
        TextArgs(
          invoice.displayTaxType,
          width: 4,
          styles: PosStyles(align: PosAlign.right),
        ),
      ]);
      //
      bytes += _texts([
        TextArgs(
          invoice.displayRandomNumber,
          width: 5,
          styles: PosStyles(align: PosAlign.left),
        ),
        TextArgs(
          invoice.displayTotalAmount,
          width: 7,
          styles: PosStyles(align: PosAlign.right),
        ),
      ]);
      //
      bytes += _lr(invoice.displaySeller, invoice.displayBuyer);
      // FIXME: barcode
      // Print barcode
      // final List<int> barData = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 4];
      // printer.barcode(Barcode.upcA(barData));
      final cahrs = invoice.barcode.split('');
      bytes += barcode(
        Barcode.code39(cahrs),
        textPos: BarcodeText.none,
        // font: BarcodeFont.fontA,
        // height: 70,
        // width: 1,
      );
      // FIXME: qr code
      // code1: invoice.leftQrString,
      // code2: invoice.rightQrString,
      // modulesize: 4,
      // errorlevel: ErrorLevel.L,
    }
    return bytes;
  }

  List<int> _header(String val) {
    final line = List.filled(6, '-').join();
    return _texts([
      TextArgs(
        line,
        width: 3,
        styles: PosStyles.defaults(
          align: PosAlign.left,
        ),
      ),
      TextArgs(
        val,
        width: 6,
        styles: PosStyles.defaults(
          align: PosAlign.center,
        ),
      ),
      TextArgs(
        line,
        width: 3,
        styles: PosStyles.defaults(
          align: PosAlign.right,
        ),
      ),
    ]);
  }

  List<int> invoiceItems(Invoice val) {
    var bytes = <int>[];
    bytes += _header('銷貨明細表');
    bytes += _items(val.items, PaperSize.mm58);
    bytes += _lr('合計', val.displayItemsCount, PaperSize.mm58);
    if (true == val.showTax) {
      bytes += _lr('應稅銷售額', val.displayItemsTXSalesAmount, PaperSize.mm58);
      bytes += _lr('免稅銷售額', val.displayItemsFreeSalesAmount, PaperSize.mm58);
    } else {
      bytes += _lr('銷售額', val.displayItemsSalesAmount, PaperSize.mm58);
    }
    if (true == val.showTax) {
      bytes += _lr('稅額', val.displayItemsTaxAmount, PaperSize.mm58);
    }
    bytes += _lr('總計', val.displayItemsTotalAmount, PaperSize.mm58);
    if (true == val.showTax) {
      // bytes += hr(ch: '─');
      bytes += hr58();
      bytes += _lr('總計', val.displayItemsTotalAmount, PaperSize.mm58);
    }
    return bytes;
  }

  ///
  /// FIXME: 列印消費明細
  ///
  List<int> receiptLite(Receipt receipt, [PaperSize paperSize]) {
    final mm58 = PaperSize.mm58.value == paperSize.value;
    final widthSize = mm58 ? PosTextSize.size1 : PosTextSize.size2;
    final heightSize = mm58 ? PosTextSize.size1 : PosTextSize.size2;
    var bytes = <int>[];
    bytes += _text(TextArgs(
      '消費明細',
      styles: PosStyles(
        bold: true,
        // fontType: PosFontType.fontB,
        width: widthSize,
        height: heightSize,
        align: PosAlign.center,
      ),
    ));
    // 自取 007
    bytes += _text(TextArgs(
      '${receipt.displayType} ${receipt.orderNumberSimply} ${receipt.seat ?? ''}',
      styles: PosStyles(
        reverse: true,
        width: widthSize,
        height: heightSize,
      ),
    ));
    bytes += _text(TextArgs(
      '列印時間：${receipt.displayPrintAt} ${receipt.userName}',
      styles: PosStyles(
        width: PosTextSize.size1,
        height: heightSize,
      ),
    ));
    bytes += _text(TextArgs(
      '訂單編號：${receipt.orderNumber}',
      styles: PosStyles(
        width: PosTextSize.size1,
        height: heightSize,
      ),
    ));
    if (receipt.displayCheckoutAt != null &&
        receipt.displayCheckoutAt.isNotEmpty) {
      bytes += _text(TextArgs(
        '結帳時間：${receipt.displayCheckoutAt}',
        styles: PosStyles(
          width: PosTextSize.size1,
          height: heightSize,
        ),
      ));
    }
    if (receipt.invoiceNumber != null && receipt.invoiceNumber.isNotEmpty) {
      bytes += _text(TextArgs(
        '發票號碼：${receipt.invoiceNumber} ${receipt.displayInvoiceStatus}',
        styles: PosStyles(
          width: widthSize,
          height: heightSize,
        ),
      ));
    }
    if (receipt.vatNumber != null && receipt.vatNumber.isNotEmpty) {
      bytes += _text(TextArgs(
        '統一編號：${receipt.vatNumber}',
        styles: PosStyles(
          width: widthSize,
          height: heightSize,
        ),
      ));
    }
    if (receipt.carrierId != null && receipt.carrierId.isNotEmpty) {
      bytes += _text(TextArgs(
        '手機條碼載具：${receipt.carrierId}',
        styles: PosStyles(
          width: widthSize,
          height: heightSize,
        ),
      ));
    }
    if (receipt.npoBan != null && receipt.npoBan.isNotEmpty) {
      bytes += _text(TextArgs(
        '愛心碼：${receipt.npoBan}',
        styles: PosStyles(
          width: widthSize,
          height: heightSize,
        ),
      ));
    }
    if (receipt.paymentName != null && receipt.paymentName.isNotEmpty) {
      bytes += _text(TextArgs(
        '支付方式：${receipt.paymentName}',
        styles: PosStyles(
          width: PosTextSize.size1,
          height: heightSize,
        ),
      ));
    }
    bytes += mm58 ? hr58() : hr80();
    bytes += _text(TextArgs(
      '合計 ${receipt.billCount}訂單，${receipt.normalItemsCount}項，${receipt.displaySubtotal}元',
      styles: PosStyles(
        width: widthSize,
        height: heightSize,
      ),
    ));
    bytes += mm58 ? hr58() : hr80();
    if (receipt.storeType.isDinner) {
      bytes += _text(TextArgs(
        '商品小計:${receipt.displaySubtotal}  服務費:${receipt.displayServiceFee}',
        styles: PosStyles(
          width: PosTextSize.size1,
          height: heightSize,
        ),
      ));
      // bytes += _texts([
      //   TextArgs(
      //     '商品小計:${receipt.displaySubtotal}',
      //     width: 6,
      //     styles: PosStyles(
      //       align: PosAlign.left,
      //       width: widthSize,
      //       height: heightSize,
      //     ),
      //   ),
      //   TextArgs(
      //     '服務費:${receipt.displayServiceFee}',
      //     width: 6,
      //     styles: PosStyles(
      //       align: PosAlign.left,
      //       width: widthSize,
      //       height: heightSize,
      //     ),
      //   ),
      // ]);
    }
    if (receipt.storeType.isRetail) {
      bytes += _text(TextArgs(
        '商品小計:${receipt.displaySubtotal}  運費:${receipt.displayDeliveryFee}',
        styles: PosStyles(
          width: PosTextSize.size1,
          height: heightSize,
        ),
      ));
      // bytes += _texts([
      //   TextArgs(
      //     '商品小計:${receipt.displaySubtotal}',
      //     width: 6,
      //     styles: PosStyles(
      //       align: PosAlign.left,
      //       width: widthSize,
      //       height: heightSize,
      //     ),
      //   ),
      //   TextArgs(
      //     '運費:${receipt.displayDeliveryFee}',
      //     width: 6,
      //     styles: PosStyles(
      //       align: PosAlign.left,
      //       width: widthSize,
      //       height: heightSize,
      //     ),
      //   ),
      // ]);
    }
    bytes += _lr('現場折價:${receipt.displayDiscount}',
        '額外費用:${receipt.displayOtherFee}', paperSize);
    if (receipt.redeemMemberPoints is num && receipt.redeemMemberPoints != 0) {
      final pointDiscount = receipt.redeemMemberPoints.abs() * (-1);
      bytes += _text(TextArgs(
        '積點折抵:${pointDiscount.decimalStyle}',
        styles: PosStyles(
          width: PosTextSize.size1,
          height: heightSize,
        ),
      ));
    }
    if (receipt.storeType.isRetail) {
      bytes += _text(TextArgs(
        '金流手續費:${receipt.displayPaymentFee}',
        styles: PosStyles(
          width: PosTextSize.size1,
          height: heightSize,
        ),
      ));
    }
    bytes += mm58 ? hr58() : hr80();
    bytes += _text(TextArgs(
      '商品總價：${receipt.displayTotal}元',
      styles: PosStyles(
        bold: true,
        width: widthSize,
        height: heightSize,
      ),
    ));
    bytes += _lr(
        '實收:${receipt.displayPaid}', '找零:${receipt.displayChange}', paperSize);
    // bytes += feed(1);
    // bytes += cut();
    return bytes;
  }

  ///
  /// 列印商品明細 (前結用)
  ///
  List<int> receiptItem(Receipt receipt, [PaperSize paperSize]) {
    final mm58 = PaperSize.mm58.value == paperSize.value;
    final widthSize = mm58 ? PosTextSize.size1 : PosTextSize.size2;
    final heightSize = mm58 ? PosTextSize.size1 : PosTextSize.size2;
    var bytes = <int>[];
    bytes += _text(TextArgs(
      '${receipt.storeType.receipt}明細',
      styles: PosStyles(
        bold: true,
        align: PosAlign.center,
        width: widthSize,
        height: heightSize,
      ),
    ));
    // 自取 007
    bytes += _text(TextArgs(
      '${receipt.displayType} ${receipt.orderNumberSimply} ${receipt.seat ?? ''}',
      styles: PosStyles(
        reverse: true,
        width: widthSize,
        height: heightSize,
      ),
    ));
    bytes += _text(TextArgs(
      '訂單編號：${receipt.orderNumber ?? ''}',
      styles: PosStyles(
        width: PosTextSize.size1,
        height: heightSize,
      ),
    ));
    bytes += _text(TextArgs(
      '訂單時間：${receipt.displayCreatedAt}',
      styles: PosStyles(
        width: PosTextSize.size1,
        height: heightSize,
      ),
    ));
    bytes += _text(TextArgs(
      '訂單狀態：${receipt.displayStatus}',
      styles: PosStyles(
        width: PosTextSize.size1,
        height: heightSize,
      ),
    ));
    bytes += _text(TextArgs(
      '付款狀態：${receipt.displayPaymentStatus}',
      styles: PosStyles(
        width: PosTextSize.size1,
        height: heightSize,
      ),
    ));
    // 餐飲 - 列出用餐時間
    if (receipt.storeType.isDinner) {
      bytes += _text(TextArgs(
        '${receipt.displayCategory}時間：${receipt.displayMealAt}',
        styles: PosStyles(
          width: PosTextSize.size1,
          height: heightSize,
        ),
      ));
    }
    if (receipt.buyerName != null && receipt.buyerName.isNotEmpty) {
      bytes += _text(TextArgs(
        '${receipt.target}姓名：${receipt.buyerName ?? ''}',
        styles: PosStyles(
          width: PosTextSize.size1,
          height: heightSize,
        ),
      ));
    }
    if (receipt.buyerPhone != null && receipt.buyerPhone.isNotEmpty) {
      bytes += _text(TextArgs(
        '${receipt.target}電話：${receipt.buyerPhone ?? ''}',
        styles: PosStyles(
          width: PosTextSize.size1,
          height: heightSize,
        ),
      ));
    }
    if (receipt.buyerAddress != null &&
        receipt.buyerAddress.isNotEmpty &&
        receipt.needAddress) {
      bytes += _text(TextArgs(
        '${receipt.target}地址：${receipt.buyerAddress ?? ''}',
        styles: PosStyles(
          width: PosTextSize.size1,
          height: heightSize,
        ),
      ));
    }
    if (receipt.displayBuyerMemo != null &&
        receipt.displayBuyerMemo.isNotEmpty) {
      bytes += _text(TextArgs(
        '顧客備註：${receipt.displayBuyerMemo ?? ''}',
        styles: PosStyles(
          width: PosTextSize.size1,
          height: heightSize,
        ),
      ));
    }
    bytes += mm58 ? hr58() : hr80();
    bytes += _items(receipt.items, paperSize);
    bytes += mm58 ? hr58() : hr80();
    bytes += _text(TextArgs(
      '小計：${receipt.normalItemsCount}項 金額：${receipt.displayTotal}元',
      styles: PosStyles(
        align: PosAlign.right,
        width: widthSize,
        height: heightSize,
      ),
    ));
    // if (receipt.servicePercentage != null && receipt.servicePercentage != 0) {
    //   bytes += _text(TextArgs(
    //     '(本次消費服務費${receipt.servicePercentage.round()}%另計)',
    //     styles: PosStyles(
    //       align: PosAlign.center,
    //       width: widthSize,
    //       height: heightSize,
    //     ),
    //   ));
    // }
    // FIXME: QR
    if (receipt.orderId != null) {
      final jsonObject = {
        'type': 'order',
        'order_id': receipt.orderId,
      };
      bytes += feed(1);
      bytes += qrcode(
        jsonEncode(jsonObject),
        size: QRSize.Size4,
        cor: QRCorrection.L,
      );
    }
    // bytes += feed(1);
    // bytes += cut();
    return bytes;
  }

  ///
  /// FIXME: 當日營收統計
  ///
  // List<int> reportsStatements(PrintStatements data) {
  //   var bytes = <int>[];
  //   return bytes;
  // }

  ///
  /// FIXME: 當日商品銷售統計
  ///
  // List<int> reportsSales(PrintSales data) {
  //   var bytes = <int>[];
  //   return bytes;
  // }

  ///
  /// FIXME: 列印折讓單
  ///

  ///
  /// 工作單
  ///
  List<int> sticker(Sticker sticker, [PaperSize paperSize]) {
    final mm58 = PaperSize.mm58.value == paperSize.value;
    final widthSize = mm58 ? PosTextSize.size1 : PosTextSize.size2;
    final heightSize = mm58 ? PosTextSize.size1 : PosTextSize.size2;

    var bytes = <int>[];
    // title
    bytes += _text(TextArgs(
      '${sticker.headerTitleNum}【${sticker.headerTitleBlockText}】${sticker.headerTitleNote}',
      styles: PosStyles(
        width: widthSize,
        height: heightSize,
      ),
    ));
    // subtitle
    bytes += _text(TextArgs(
      '${sticker.headerSubTitle}  ${sticker?.headerSubTitleNote}',
      styles: PosStyles(
        width: widthSize,
        height: heightSize,
      ),
    ));
    // divider
    bytes += mm58 ? hr58() : hr80();
    // body title
    bytes += _text(TextArgs(
      sticker?.bodyTitle ?? '',
      styles: PosStyles(
        width: widthSize,
        height: heightSize,
      ),
    ));
    // body subtitle
    bytes += _text(TextArgs(
      sticker?.bodySubTitle ?? '',
      styles: PosStyles(
        width: widthSize,
        height: heightSize,
      ),
    ));
    // bytes += feed(1);
    // bytes += cut();
    return bytes;
  }

  List<int> _items(Iterable<Item> it, [PaperSize paperSize]) {
    return it.fold(<int>[], (previousValue, element) {
      previousValue += _item(element, paperSize);
      return previousValue;
    });
  }

  List<int> _item(Item item, [PaperSize paperSize]) {
    final mm58 = PaperSize.mm58.value == paperSize.value;
    final widthSize = mm58 ? PosTextSize.size1 : PosTextSize.size2;
    final heightSize = mm58 ? PosTextSize.size1 : PosTextSize.size2;
    var bytes = <int>[];
    bytes += _text(TextArgs(
      item.itemName,
      styles: PosStyles.defaults(
        align: PosAlign.left,
        width: widthSize,
        height: heightSize,
      ),
    ));
    if (item.comment != null && item.comment.isNotEmpty) {
      bytes += _text(TextArgs(
        '  ${item.comment}',
        styles: PosStyles.defaults(
          align: PosAlign.left,
          width: PosTextSize.size1,
          height: heightSize,
        ),
      ));
    }
    // FIXME:
    // bytes += _lr(item.leftString, '${item.displayTotal}元');
    bytes += _text(TextArgs(
      '${item.leftString} = ${item.displayTotal}元',
      styles: PosStyles.defaults(
        width: widthSize,
        height: heightSize,
      ),
    ));

    return bytes;
  }

  Future<List<int>> textImage(String text) async {
    final imgBytes = await _generateImageFromString(text, TextAlign.left);
    final img = decodeImage(imgBytes);
    return image(img);
  }

  ///
  /// https://stackoverflow.com/a/70845029
  ///
  Future<Uint8List> _generateImageFromString(
    String text,
    ui.TextAlign align,
  ) async {
    ui.PictureRecorder recorder = new ui.PictureRecorder();
    Canvas canvas = Canvas(
        recorder,
        Rect.fromCenter(
          center: Offset(0, 0),
          width: 370,
          height: 400, // cheated value, will will clip it later...
        ));
    TextSpan span = TextSpan(
      style: const TextStyle(
        color: Colors.black,
        fontSize: 20,
        fontWeight: ui.FontWeight.bold,
      ),
      text: text,
    );
    TextPainter tp = TextPainter(
        text: span,
        maxLines: 3,
        textAlign: align,
        textDirection: TextDirection.ltr);
    tp.layout(minWidth: 370, maxWidth: 370);
    tp.paint(canvas, const Offset(0.0, 0.0));
    var picture = recorder.endRecording();
    final pngBytes = await picture.toImage(
      tp.size.width.toInt(),
      tp.size.height.toInt() - 2, // decrease padding
    );
    final byteData = await pngBytes.toByteData(format: ui.ImageByteFormat.png);
    return byteData.buffer.asUint8List();
  }
}

extension ExtensionTextArgs on TextArgs {
  PosColumn asPosColumn() {
    return PosColumn(
      // text: null,
      textEncoded: utf8.encode(text ?? ''),
      width: width,
      styles: styles ?? PosStyles.defaults(),
      containsChinese: containsChinese,
    );
  }
}

// extension ExtensionBluetoothDevice on BluetoothDevice {
//   SettingLabel asSettingLabel() {
//     return SettingLabel(
//       categoryIds: <num>[],
//       id: 0,
//       ip: "",
//       macAddress: address,
//       name: name,
//       printCount: Switcher.On.index,
//       status: Switcher.On.index,
//     );
//   }
// }

extension ExtensionPrinterBluetooth on PrinterBluetooth {
  SettingLabel asSettingLabel() {
    return SettingLabel(
      categoryIds: <num>[],
      id: 0,
      ip: address,
      macAddress: '${PrinterType.bth.value}@$address',
      name: name,
      printCount: 1,
      status: Switcher.On.index,
    );
  }
}

extension ExtensionNetworkAddress on NetworkAddress {
  SettingLabel asSettingLabel(num port) {
    final ipAddress = '$ip:$port';
    return SettingLabel(
      categoryIds: <num>[],
      id: 0,
      ip: ipAddress,
      // port: '$port',
      name: ip,
      macAddress: '${PrinterType.net.value}@$ipAddress',
      printCount: 1,
      status: Switcher.On.index,
    );
  }
}

// extension _ExtensionReceipt on Receipt {
//   Iterable<Sticker> asStickers() {
//     final json = toJson();
//     return items.map((item) {
//       final sticker = Sticker.fromJson(json);
//       sticker.itemName = item.itemName;
//       sticker.comment = item.comment;
//       sticker.unitPrice = item.unitPrice;
//       return sticker;
//     });
//   }
// }
