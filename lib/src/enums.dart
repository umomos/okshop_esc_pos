class PrintType {
  final num value;
  const PrintType._internal(this.value);
  // 發票
  static final invoice = PrintType._internal('invoice'.hashCode);
  // 消費明細
  static final receiptLite = PrintType._internal('receipt_lite'.hashCode);
  // 商品明細
  static final receiptItem = PrintType._internal('receipt_item'.hashCode);
  // 工作單
  static final sticker = PrintType._internal('sticker'.hashCode);
  // 日結單
  static final reportStatements =
      PrintType._internal('report_statements'.hashCode);
  // 商品統計
  static final reportSales = PrintType._internal('report_sales'.hashCode);
}

class PrinterType {
  final String value;
  const PrinterType._internal(this.value);
  static final all = <String>[net.value, bth.value];
  // 網路印表機
  static final net = PrinterType._internal('esc');
  // 藍牙印表機
  static final bth = PrinterType._internal('bth');
}

class Option {
  final num value;
  const Option._internal(this.value);
  // Escape
  static final esc = Option._internal(0x1B);
  // Text delimiter
  static final fs = Option._internal(0x1C);
  // Group separator
  static final gs = Option._internal(0x1D);
  // data link escape
  static final dle = Option._internal(0x10);
  // End of transmission
  static final eot = Option._internal(0x04);
  // Enquiry character
  static final enq = Option._internal(0x05);
  // Spaces
  static final sp = Option._internal(0x20);
  // Horizontal list
  static final ht = Option._internal(0x09);
  // Print and wrap (horizontal orientation)
  static final lf = Option._internal(0x0A);
  // Home key
  static final cr = Option._internal(0x0D);
  // Carriage control (print and return to the standard mode (in page mode))
  static final ff = Option._internal(0x0C);
  // Canceled (cancel print data in page mode)
  static final can = Option._internal(0x18);
}
