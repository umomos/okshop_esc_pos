# okshop_esc_pos

A new flutter plugin project.

## Getting Started

* [ESC/POS 控制指令](https://www.codeprj.com/zh/blog/6002381.html)
    很長的指令列表
* [Android之打印](https://www.twblogs.net/a/5e4e4fdbbd9eee101e823977)
    很長的指令列表 (JAVA 含範例，推薦)
* [設置 utf-8, code page 方式](https://stackoverflow.com/a/63548498)
    [28, 40, 67, 2, 0, 48, 1]
* [epson](https://reference.epson-biz.com/modules/ref_escpos/index.php?content_id=32)
    epson 官方網頁指令
* [文字截圖列印](https://stackoverflow.com/a/70845029)

## package

* [sunmi_printer_plus](https://pub.dev/packages/sunmi_printer_plus)
* [pos_printer_manager](https://pub.dev/packages/pos_printer_manager)
* [](https://pub.dev/packages/flutter_esc_printer)

### 藍牙

* [藍牙](https://pub.dev/packages/esc_pos_bluetooth)
* [129 LIKES](https://pub.dev/packages/blue_thermal_printer)
* [21 LIKES](https://pub.dev/packages/bluetooth_thermal_printer)
* [87 LIKES](https://pub.dev/packages/bluetooth_print)
* [38 LIKES](https://pub.dev/packages/flutter_bluetooth_basic)
* [16 LIKES](https://pub.dev/packages/blue_print_pos)

## 列印發票指令碼

* [Android 連接印表機產生QRCode](https://dearsherlock.github.io/star%20printer/2014/11/05/starsdk-to-generate-qrcode.html)
* [RS232 與 ESC／POS](http://blog.udn.com/oliverozane/130576928)

## esc like

* [esc_pos_utils_plus](https://pub.dev/packages/esc_pos_utils_plus)
* [esc_pos_gen](https://pub.dev/packages/esc_pos_gen)
* [pos_printer_manager](https://pub.dev/packages/pos_printer_manager)

## sunmi printer

* [sunmi_printer](https://pub.dev/packages/sunmi_printer)
* [sunmi_printer_plus](https://pub.dev/packages/sunmi_printer_plus)

## 文字截圖

* [Flutter 31: 图解 TextPainter 与 TextSpan 小尝试
](https://www.jianshu.com/p/0fd1eaea6269)
