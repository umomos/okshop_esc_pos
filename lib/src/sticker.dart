// To parse this JSON data, do
//
//     final sticker = stickerFromJson(jsonString);

import 'dart:convert';

class Sticker {
  Sticker({
    this.orderNumber,
    this.type,
    this.meatAt,
    this.seat,
    this.itemName,
    this.comment,
    this.unitPrice,
  });

  String orderNumber;
  num type;
  DateTime meatAt;
  String seat;
  String itemName;
  String comment;
  num unitPrice;

  Sticker copyWith({
    String orderNumber,
    num type,
    DateTime meatAt,
    String seat,
    String itemName,
    String comment,
    num unitPrice,
  }) =>
      Sticker(
        orderNumber: orderNumber ?? this.orderNumber,
        type: type ?? this.type,
        meatAt: meatAt ?? this.meatAt,
        seat: seat ?? this.seat,
        itemName: itemName ?? this.itemName,
        comment: comment ?? this.comment,
        unitPrice: unitPrice ?? this.unitPrice,
      );

  factory Sticker.fromRawJson(String str) => Sticker.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Sticker.fromJson(Map<String, dynamic> json) => Sticker(
        orderNumber: json["order_number"] == null ? null : json["order_number"],
        type: json["type"] == null ? null : json["type"],
        meatAt:
            json["meat_at"] == null ? null : DateTime.parse(json["meat_at"]),
        seat: json["seat"] == null ? null : json["seat"],
        itemName: json["item_name"] == null ? null : json["item_name"],
        comment: json["comment"] == null ? null : json["comment"],
        unitPrice: json["unit_price"] == null ? null : json["unit_price"],
      );

  Map<String, dynamic> toJson() => {
        "order_number": orderNumber == null ? null : orderNumber,
        "type": type == null ? null : type,
        "meat_at": meatAt == null ? null : meatAt.toIso8601String(),
        "seat": seat == null ? null : seat,
        "item_name": itemName == null ? null : itemName,
        "comment": comment == null ? null : comment,
        "unit_price": unitPrice == null ? null : unitPrice,
      };
}
