import 'dart:async';

import 'package:esc_pos_bluetooth/esc_pos_bluetooth.dart';
import 'package:stream_transform/stream_transform.dart';
import 'package:okshop_model/okshop_model.dart';
import 'package:ping_discover_network/ping_discover_network.dart';
import 'package:wifi/wifi.dart';

import 'constants.dart';
import 'enums.dart';
import 'extension.dart';

class PrinterRepository {
  final _printerManager = PrinterBluetoothManager();
  final _timeout = 4;

  static Future<Iterable<SettingLabel>> _lanScan(
      [num port = kDefaultPort]) async {
    String ip;
    try {
      ip = await Wifi.ip;
      print('local ip:\t$ip');
    } catch (e) {
      throw e;
    }
    final completer = Completer<Iterable<SettingLabel>>();
    final list = <SettingLabel>[];
    final subnet = ip.substring(0, ip.lastIndexOf('.'));
    // final port = 9100;
    final stream = NetworkAnalyzer.discover2(subnet, port);
    stream.where((event) => event.exists).timeout(Duration(seconds: 4)).listen(
      (event) {
        print('[PrinterRepository] ip(${event.ip}), exist(${event.exists})');
        list.add(event.asSettingLabel(port));
      },
      onDone: () {
        print('[PrinterRepository] onDone(${list.length})');
        completer.complete(list);
      },
      onError: (error, stackTrace) {
        print('[PrinterRepository] onError');
        // completer.completeError(error, stackTrace);
      },
    );
    return completer.future;
  }

  Future<Iterable<SettingLabel>> scan([List<PrinterType> types]) {
    types ??= [PrinterType.net, PrinterType.bth];
    final completer = Completer<Iterable<SettingLabel>>();
    final futures = <Future<Iterable<SettingLabel>>>[];
    if (types.contains(PrinterType.net)) {
      futures.add(_lanScan());
    }
    if (types.contains(PrinterType.bth)) {
      futures.add(_bluetoothScen());
    }
    Future.wait(futures).then(
      (value) {
        final list = value.fold<List<SettingLabel>>(<SettingLabel>[],
            (previousValue, element) {
          previousValue.addAll(element);
          // previousValue.addAll(<SettingLabel>[...element]);
          return previousValue;
        });
        final entries = list.map((e) => MapEntry(e.uuid, e));
        final map = Map.fromEntries(entries);
        final it = map.values;
        print('[PrinterRepository] scan(${it.length})');
        completer.complete(it);
      },
    );
    return completer.future;
  }

  Future<Iterable<SettingLabel>> _bluetoothScen() {
    final completer = Completer<Iterable<SettingLabel>>();
    // _printerManager.isScanningStream.listen((event) {
    //   print('[PrinterRepository] isScanningStream($event)');
    // });
    final map = <num, SettingLabel>{};
    _printerManager.scanResults.debounce(Duration(seconds: 1)).listen(
      (devices) {
        print('[PrinterRepository] scanResults(${devices.length})');
        // 特殊: 3 代表印表機
        final entries = devices.where((element) => 3 == element.type).map(
          (e) {
            final item = e.asSettingLabel();
            return MapEntry(item.uuid, item);
          },
        );
        map.addEntries(entries);
      },
    );
    Timer(Duration(seconds: _timeout + 2), () {
      final it = map.values;
      print('[PrinterRepository] _bluetoothScen(${it.length})');
      completer.complete(it);
    });
    _printerManager.startScan(Duration(seconds: _timeout));
    // _printerManager.stopScan();
    return completer.future;
  }
}
