library okshop_esc_pos;

export 'src/printer_repository.dart';
export 'src/extension.dart';
export 'src/constants.dart';
export 'src/enums.dart';
// export 'src/sticker.dart';

import 'dart:async';

import 'package:flutter/services.dart';

class OkshopEscPos {
  static const MethodChannel _channel = const MethodChannel('okshop_esc_pos');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
